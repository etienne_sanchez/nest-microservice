import { Injectable } from '@nestjs/common';
import { ItemDTO } from './dto/item.dto';
import { Item } from './entities/item.entity';

@Injectable()
export class ItemMapper {
  dtoToEntity(dto: ItemDTO): Item {
    return new Item(dto.isActive, dto.createdBy, dto.lastChangedBy);
  }

  entityToDto(item: Item): ItemDTO {
    return new ItemDTO(item.isActive, item.createdBy, item.lastChangedBy);
  }
}
