import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ItemDTO } from './dto/item.dto';
import { Item } from './entities/item.entity';
import { ItemMapper } from './items.mapper';

@Injectable()
export class ItemsService {
  constructor(
    @InjectRepository(Item) private readonly repo: Repository<Item>,
    private readonly itemMapper: ItemMapper,
  ) {}

  create(createItemDto: ItemDTO): Promise<ItemDTO> {
    return this.repo
      .save(this.itemMapper.dtoToEntity(createItemDto))
      .then((e) => this.itemMapper.entityToDto(e));
  }

  public async findAll() {
    return await this.repo.find();
  }

  public async findOne(id: string) {
    return await this.repo.findOne(id);
  }

  public async update(id: string, updateItemDto: ItemDTO) {
    return await this.repo.update(id, updateItemDto);
  }

  public async remove(id: string) {
    const entityToRemove = await this.repo.findOne(id);
    return await this.repo.remove(entityToRemove);
  }
}
