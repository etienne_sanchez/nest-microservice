export class ItemDTO implements Readonly<ItemDTO> {
  constructor(isActive: boolean, createdBy: string, lastChangedBy: string) {
    this.isActive = isActive;
    this.createdBy = createdBy;
    this.lastChangedBy = lastChangedBy;
  }
  id: string;
  createdBy: string;
  isActive: boolean;
  lastChangedBy: string;
}
